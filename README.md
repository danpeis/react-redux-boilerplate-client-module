
```
#!javascript


```
# React Redux Boilerplate [Client Module]

## Folders
- actions: Define the actions for the module  
  Example file (`actions/actions.js`):
  ``` #!javascript
  import m from '../';
  export default {
    action : m.ActionCreator({
      ActionName: 'TEST_ACTION', // Action name, renames to [ModuleName]_[ActionName]
      Validation: t => t.struct({
        id: t.String,
        value: t.Number
      }), // tcomb validation | null = no validation,
      Action: value => value // Optional: default is identity
    })
  };
  ```
- components: Define the actual components of the module  
  Example file (`components/RootComponent.jsx`):
  ``` javascript
  import React, { PropTypes } from 'react';
  import { connect } from 'react-redux';
  import actions from '../actions/actions';

  @connect(globalState => { componentProps : {}})
  class RootComponent extends React.Component {
    render() {
      console.log(this.props);
      return (
        <div>
          <button onClick={() => this.props.dispatch(actions
            .action({id: this.props.id, value: 2}))}>Pulsar</button>
          {this.props.children}
        </div>
      );
    }
  }

  export default RootComponent;
  ```
- reducers: Define module reducers in this folder  
  Example file (`reducers/index.js`):
  ``` javascript
  import { action as myAction } from '../actions/actions';
  import Immutable from 'immutable';

  export default (state = Immutable.fromJS({value: {}}), action) => {
    if (action.type == myAction.type) {
      return state.setIn(['value', action.payload.id], action.payload.value);
    }
    return state;
  };
  ```

### Files
- `index.js` __required__: Module metadata
  ``` javascript
  export default {
    ModuleName: 'Main',
    Version: '0.0',
    Dependencies : []
  };
  ```
- `router.jsx`: Define module routing here
  ``` javascript
  import React from 'react';
  import { Route } from 'react-router';

  import RootComponent from './components/RootComponent';

  const router = (
    <Route path="/" component={RootComponent}/>
  );

  export default router;
  ```